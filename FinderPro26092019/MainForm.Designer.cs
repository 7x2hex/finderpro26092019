﻿namespace FinderPro26092019
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.FileNameLabel = new System.Windows.Forms.Label();
            this.FileNameTextBox = new System.Windows.Forms.TextBox();
            this.StartDirectoryLabel = new System.Windows.Forms.Label();
            this.StartDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.StartDirectorySelectButton = new System.Windows.Forms.Button();
            this.TextInsideLabel = new System.Windows.Forms.Label();
            this.TextInsideTextBox = new System.Windows.Forms.TextBox();
            this.StartSearchButton = new System.Windows.Forms.Button();
            this.ResultsFastTree = new FastTreeNS.FastTree();
            this.PauseSearchButton = new System.Windows.Forms.Button();
            this.StopSearchButton = new System.Windows.Forms.Button();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.unicodeRadioButton = new System.Windows.Forms.RadioButton();
            this.asciiRadioButton = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // FileNameLabel
            // 
            this.FileNameLabel.AutoSize = true;
            this.FileNameLabel.Location = new System.Drawing.Point(12, 9);
            this.FileNameLabel.Name = "FileNameLabel";
            this.FileNameLabel.Size = new System.Drawing.Size(384, 13);
            this.FileNameLabel.TabIndex = 0;
            this.FileNameLabel.Text = "Какой файл ищем? (* множество любых символов | ? один любой символ)";
            // 
            // FileNameTextBox
            // 
            this.FileNameTextBox.Location = new System.Drawing.Point(12, 25);
            this.FileNameTextBox.Name = "FileNameTextBox";
            this.FileNameTextBox.Size = new System.Drawing.Size(487, 20);
            this.FileNameTextBox.TabIndex = 1;
            this.FileNameTextBox.Text = "?.*";
            // 
            // StartDirectoryLabel
            // 
            this.StartDirectoryLabel.AutoSize = true;
            this.StartDirectoryLabel.Location = new System.Drawing.Point(12, 51);
            this.StartDirectoryLabel.Name = "StartDirectoryLabel";
            this.StartDirectoryLabel.Size = new System.Drawing.Size(155, 13);
            this.StartDirectoryLabel.TabIndex = 2;
            this.StartDirectoryLabel.Text = "В какой директории начнём?";
            // 
            // StartDirectoryTextBox
            // 
            this.StartDirectoryTextBox.Location = new System.Drawing.Point(12, 67);
            this.StartDirectoryTextBox.Name = "StartDirectoryTextBox";
            this.StartDirectoryTextBox.ReadOnly = true;
            this.StartDirectoryTextBox.Size = new System.Drawing.Size(487, 20);
            this.StartDirectoryTextBox.TabIndex = 3;
            this.StartDirectoryTextBox.Text = "C:\\Program Files (x86)";
            // 
            // StartDirectorySelectButton
            // 
            this.StartDirectorySelectButton.Location = new System.Drawing.Point(390, 47);
            this.StartDirectorySelectButton.Name = "StartDirectorySelectButton";
            this.StartDirectorySelectButton.Size = new System.Drawing.Size(109, 20);
            this.StartDirectorySelectButton.TabIndex = 4;
            this.StartDirectorySelectButton.Text = "Выбрать";
            this.StartDirectorySelectButton.UseVisualStyleBackColor = true;
            this.StartDirectorySelectButton.Click += new System.EventHandler(this.StartDirectorySelectButton_Click);
            // 
            // TextInsideLabel
            // 
            this.TextInsideLabel.AutoSize = true;
            this.TextInsideLabel.Location = new System.Drawing.Point(12, 93);
            this.TextInsideLabel.Name = "TextInsideLabel";
            this.TextInsideLabel.Size = new System.Drawing.Size(140, 13);
            this.TextInsideLabel.TabIndex = 5;
            this.TextInsideLabel.Text = "Что может быть в файле?";
            // 
            // TextInsideTextBox
            // 
            this.TextInsideTextBox.Location = new System.Drawing.Point(12, 109);
            this.TextInsideTextBox.Name = "TextInsideTextBox";
            this.TextInsideTextBox.Size = new System.Drawing.Size(487, 20);
            this.TextInsideTextBox.TabIndex = 6;
            // 
            // StartSearchButton
            // 
            this.StartSearchButton.Location = new System.Drawing.Point(194, 162);
            this.StartSearchButton.Name = "StartSearchButton";
            this.StartSearchButton.Size = new System.Drawing.Size(42, 23);
            this.StartSearchButton.TabIndex = 10;
            this.StartSearchButton.Text = "|>";
            this.StartSearchButton.UseVisualStyleBackColor = true;
            this.StartSearchButton.Click += new System.EventHandler(this.StartSearchButton_Click);
            // 
            // ResultsFastTree
            // 
            this.ResultsFastTree.AutoScroll = true;
            this.ResultsFastTree.AutoScrollMinSize = new System.Drawing.Size(0, 59);
            this.ResultsFastTree.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ResultsFastTree.HotTracking = true;
            this.ResultsFastTree.ImageCheckBoxOff = ((System.Drawing.Image)(resources.GetObject("ResultsFastTree.ImageCheckBoxOff")));
            this.ResultsFastTree.ImageCheckBoxOn = ((System.Drawing.Image)(resources.GetObject("ResultsFastTree.ImageCheckBoxOn")));
            this.ResultsFastTree.ImageCollapse = ((System.Drawing.Image)(resources.GetObject("ResultsFastTree.ImageCollapse")));
            this.ResultsFastTree.ImageDefaultIcon = ((System.Drawing.Image)(resources.GetObject("ResultsFastTree.ImageDefaultIcon")));
            this.ResultsFastTree.ImageExpand = ((System.Drawing.Image)(resources.GetObject("ResultsFastTree.ImageExpand")));
            this.ResultsFastTree.IsEditMode = false;
            this.ResultsFastTree.Location = new System.Drawing.Point(515, 25);
            this.ResultsFastTree.Name = "ResultsFastTree";
            this.ResultsFastTree.ShowExpandBoxes = true;
            this.ResultsFastTree.ShowRootNode = true;
            this.ResultsFastTree.Size = new System.Drawing.Size(584, 476);
            this.ResultsFastTree.TabIndex = 11;
            // 
            // PauseSearchButton
            // 
            this.PauseSearchButton.Enabled = false;
            this.PauseSearchButton.Location = new System.Drawing.Point(242, 162);
            this.PauseSearchButton.Name = "PauseSearchButton";
            this.PauseSearchButton.Size = new System.Drawing.Size(42, 23);
            this.PauseSearchButton.TabIndex = 12;
            this.PauseSearchButton.Text = "| |";
            this.PauseSearchButton.UseVisualStyleBackColor = true;
            this.PauseSearchButton.Click += new System.EventHandler(this.PauseSearchButton_Click);
            // 
            // StopSearchButton
            // 
            this.StopSearchButton.Enabled = false;
            this.StopSearchButton.Location = new System.Drawing.Point(290, 162);
            this.StopSearchButton.Name = "StopSearchButton";
            this.StopSearchButton.Size = new System.Drawing.Size(42, 23);
            this.StopSearchButton.TabIndex = 13;
            this.StopSearchButton.Text = "||||";
            this.StopSearchButton.UseVisualStyleBackColor = true;
            this.StopSearchButton.Click += new System.EventHandler(this.StopSearchButton_Click);
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Location = new System.Drawing.Point(12, 203);
            this.StatusLabel.MaximumSize = new System.Drawing.Size(400, 800);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(113, 13);
            this.StatusLabel.TabIndex = 15;
            this.StatusLabel.Text = "What is going on now:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // unicodeRadioButton
            // 
            this.unicodeRadioButton.AutoSize = true;
            this.unicodeRadioButton.Location = new System.Drawing.Point(73, 135);
            this.unicodeRadioButton.Name = "unicodeRadioButton";
            this.unicodeRadioButton.Size = new System.Drawing.Size(65, 17);
            this.unicodeRadioButton.TabIndex = 17;
            this.unicodeRadioButton.Text = "Unicode";
            this.unicodeRadioButton.UseVisualStyleBackColor = true;
            // 
            // asciiRadioButton
            // 
            this.asciiRadioButton.AutoSize = true;
            this.asciiRadioButton.Checked = true;
            this.asciiRadioButton.Location = new System.Drawing.Point(15, 135);
            this.asciiRadioButton.Name = "asciiRadioButton";
            this.asciiRadioButton.Size = new System.Drawing.Size(52, 17);
            this.asciiRadioButton.TabIndex = 16;
            this.asciiRadioButton.TabStop = true;
            this.asciiRadioButton.Text = "ASCII";
            this.asciiRadioButton.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1111, 520);
            this.Controls.Add(this.unicodeRadioButton);
            this.Controls.Add(this.asciiRadioButton);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.StopSearchButton);
            this.Controls.Add(this.PauseSearchButton);
            this.Controls.Add(this.ResultsFastTree);
            this.Controls.Add(this.StartSearchButton);
            this.Controls.Add(this.TextInsideTextBox);
            this.Controls.Add(this.TextInsideLabel);
            this.Controls.Add(this.StartDirectorySelectButton);
            this.Controls.Add(this.StartDirectoryTextBox);
            this.Controls.Add(this.StartDirectoryLabel);
            this.Controls.Add(this.FileNameTextBox);
            this.Controls.Add(this.FileNameLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Поисковик файлов";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label FileNameLabel;
        private System.Windows.Forms.TextBox FileNameTextBox;
        private System.Windows.Forms.Label StartDirectoryLabel;
        private System.Windows.Forms.Button StartDirectorySelectButton;
        private System.Windows.Forms.Label TextInsideLabel;
        private System.Windows.Forms.TextBox TextInsideTextBox;
        public System.Windows.Forms.TextBox StartDirectoryTextBox;
        private System.Windows.Forms.Button StartSearchButton;
        private FastTreeNS.FastTree ResultsFastTree;
        private System.Windows.Forms.Button PauseSearchButton;
        private System.Windows.Forms.Button StopSearchButton;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.RadioButton unicodeRadioButton;
        private System.Windows.Forms.RadioButton asciiRadioButton;
    }
}

