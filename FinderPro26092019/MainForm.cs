﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using FastTreeNS;
using FinderPro26092019.Properties;

namespace FinderPro26092019
{
    public partial class MainForm : Form
    {
        //реализация WinApi
        const UInt32 INFINITE = 0xFFFFFFFF;
        const UInt32 WAIT_ABANDONED = 0x00000080;
        const UInt32 WAIT_OBJECT_0 = 0x00000000;
        const UInt32 WAIT_TIMEOUT = 0x00000102;

        //требуется для приостановки потока до освобождения объекта
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern UInt32 WaitForSingleObject(IntPtr hHandle, UInt32 dwMilliseconds);

        //создание события
        [DllImport("kernel32.dll")]
        static extern IntPtr CreateEvent(IntPtr lpEventAttributes, bool bManualReset, bool bInitialState, string lpName);

        //установка события
        [DllImport("kernel32.dll")]
        static extern bool SetEvent(IntPtr hEvent);

        //сброс события
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool ResetEvent(IntPtr hEvent);

        //locker для синхронизации потоков во избежании неправильной работы поиска
        private readonly object NodeLocker = new object();

        private string CurrentPath;
        private int CurrentFound;
        Stopwatch CurrentTime;
        private string CurrentStatus = "Готов к работе";

        IntPtr hEvWatchResult = IntPtr.Zero;
        //поток поиска
        Task<bool> SearchThread;
        //на паузе? да ? не?
        bool Paused = false;
        //корневая директория (node)
        Node RootNode = null;
        //поиск активен? не ? да?
        bool ScanAlive = false;

        CancellationTokenSource source = null;

        public MainForm()
        {
            //создаём событие для поиска
            hEvWatchResult = CreateEvent(IntPtr.Zero, true, false, "");

            InitializeComponent();

            //подгружаем параметры поиска из настроек приложуньки
            FileNameTextBox.Text = Settings.Default["FindName"].ToString();
            StartDirectoryTextBox.Text = Settings.Default["FindDirectory"].ToString();
            TextInsideTextBox.Text = Settings.Default["FindText"].ToString();
            if ((bool)Settings.Default["ASCI"]) asciiRadioButton.Checked = true;
            else unicodeRadioButton.Checked = true;
        }

        //функция поиска по содержимому (тырнеты)
        private static Boolean FileContainsBytes(String path, Byte[] compare)
        {
            Boolean contains = false;

            Int32 blockSize = 4096;
            if ((compare.Length >= 1) && (compare.Length <= blockSize))
            {
                Byte[] block = new Byte[compare.Length - 1 + blockSize];

                try
                {
                    FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);

                    // Read the first bytes from the file into "block":
                    Int32 bytesRead = fs.Read(block, 0, block.Length);

                    do
                    {
                        // Search "block" for the sequence "compare":
                        Int32 endPos = bytesRead - compare.Length + 1;
                        for (Int32 i = 0; i < endPos; i++)
                        {
                            // Read "compare.Length" bytes at position "i" from the buffer,
                            // and compare them with "compare":
                            Int32 j;
                            for (j = 0; j < compare.Length; j++)
                            {
                                if (block[i + j] != compare[j])
                                {
                                    break;
                                }
                            }

                            if (j == compare.Length)
                            {
                                // "block" contains the sequence "compare":
                                contains = true;
                                break;
                            }
                        }

                        // Search completed?
                        if (contains || (fs.Position >= fs.Length))
                        {
                            break;
                        }
                        else
                        {
                            // Copy the last "compare.Length - 1" bytes to the beginning of "block":
                            for (Int32 i = 0; i < (compare.Length - 1); i++)
                            {
                                block[i] = block[blockSize + i];
                            }

                            // Read the next "blockSize" bytes into "block":
                            bytesRead = compare.Length - 1 + fs.Read(block, compare.Length - 1, blockSize);
                        }
                    }
                    while (false);

                    fs.Close();
                }
                catch (Exception)
                {
                }
            }

            return contains;
        }

        //Построение древесной иерархии
        private bool BuildTree(Node root, string fileCtx, string fileMask)
        {
            var toProcess = new Stack<Node>();
            toProcess.Push(root);
            while (toProcess.Count > 0)
            {
                if (!Paused)
                {
                    var node = toProcess.Pop();
                    //Console.WriteLine(node.FullPath);

                    foreach (var dir in Directory.GetDirectories(node.FullPath))
                    {
                        while (Paused) { }

                        try
                        {
                            var n = new Node { FullPath = dir };
                            lock (NodeLocker)
                            {
                                node.Add(n);
                            }
                            toProcess.Push(n);
                        }
                        catch (Exception)
                        {
                            //а к этому файлу доступа нет(
                        }
                    }

                    foreach (var file in Directory.GetFiles(node.FullPath, fileMask))
                    {
                        try
                        {
                            while (Paused) { }

                            var fInfo = new FileInfo(file);
                            if (fInfo.Length > 1073741824) //игнор , если сайз файла выше 1 гб
                                continue;

                            //чекаем на наличие содержимого
                            Encoding encoding = asciiRadioButton.Checked ? Encoding.ASCII : Encoding.Unicode;

                            if (!string.IsNullOrEmpty(fileCtx) ? FileContainsBytes(file, encoding.GetBytes(fileCtx)) : true)
                            {
                                var n = new Node { FullPath = file, IsFile = true };
                                CurrentPath = file;
                                lock (NodeLocker)
                                {
                                    node.Add(n);
                                }
                                CurrentFound++;
                            }
                        }
                        catch (Exception)
                        {
                            //а к этому файлу доступа нет(
                        }
                    }
                }
            }

            this.Invoke(new Action(() =>
            {
                CurrentStatus = "Завершён";
                StopSearchProccess();
            }));

            return true;
        }

        private async void StartSearchButton_Click(object sender, EventArgs e)
        {
            //проверяем заполненность поля с именем файла (маской)
            if (string.IsNullOrEmpty(FileNameTextBox.Text))
            {
                MessageBox.Show("Вы не ввели название файла для поиска", "Ошибка");
                return;
            }
            //проверяем верность путя
            if (!Directory.Exists(StartDirectoryTextBox.Text))
            {
                MessageBox.Show("Каталога по этому пути больше не существует...", "Ошибка");
                return;
            }
            //обнуляем переменную с обработанными файлами и fasttree
            CurrentFound = 0;

            //отключаем кнопку начала поиска и активируем паузу и остановку
            StartSearchButton.Enabled = false;
            PauseSearchButton.Enabled = true;
            StopSearchButton.Enabled = true;


            //Обозначаем корневую директорию
            RootNode = new Node { FullPath = StartDirectoryTextBox.Text };

            ////Начинаем отсчёт времени
            CurrentTime = Stopwatch.StartNew();

            ////Изменияем текущий статус поиска
            CurrentStatus = "В работе";

            source = new CancellationTokenSource();

            //Создаём поток для поиска
            var tSearchThread = Task.Run(() => {
                SetEvent(hEvWatchResult);
                return BuildTree(RootNode, TextInsideTextBox.Text, FileNameTextBox.Text);
            }, source.Token);

            SearchThread = tSearchThread;

            if (!ScanAlive)
            {
                new Thread(() =>
                {
                    while (WaitForSingleObject(hEvWatchResult, INFINITE) == WAIT_OBJECT_0)
                    {
                        if (RootNode != null && !Paused)
                        {
                            ResultsFastTree.Invoke(new MethodInvoker(() =>
                            {
                                lock (NodeLocker)
                                {
                                    ResultsFastTree.Build(RootNode);
                                    //ResultsFastTree.Invalidate();
                                }
                            }));
                            UpdateStatusLabel();
                        }
                    }
                }).Start();
                ScanAlive = true;
            }
            try
            {
                await SearchThread;

                MessageBox.Show(SearchThread.Result.ToString());
            }
            catch (Exception)
            {

            }
        }

        private async void PauseSearchButton_Click(object sender, EventArgs e)
        {
            await Task.Run(() =>
            {
                if (!Paused)
                {
                    //сброс события
                    ResetEvent(hEvWatchResult);
                    //паузим время
                    CurrentTime.Stop();
                    //ченжим статус
                    CurrentStatus = "На паузе";
                    UpdateStatusLabel();
                    Paused = true;
                }
                else
                {
                    //возобновление события
                    SetEvent(hEvWatchResult);
                    //размораживаем всё
                    Paused = false;
                    CurrentStatus = "В работе";
                    CurrentTime.Start();
                }
            });
        }

        private void StopSearchButton_Click(object sender, EventArgs e)
        {
            CurrentStatus = "Остановлен";
            StopSearchProccess();
        }

        async void StopSearchProccess()
        {
            await Task.Run(() =>
            {
                try
                {
                    //сбрасываем событие, чтобы поиск прекратился
                    ResetEvent(hEvWatchResult);

                    if (source != null)
                    {
                        source.Cancel();
                    }
                    source = null;
                    SearchThread = null;
                    RootNode = null;
                }
                catch (Exception)
                {
                    //что-то не так пошло, явно не так.........но об этом никто не узнает
                }
                //Апдейт интерфейса в связи с завершением поиска
                this.Invoke(new Action(() =>
                {
                    UpdateStatusLabel();
                    Paused = false;
                    StartSearchButton.Enabled = true;
                    PauseSearchButton.Enabled = false;
                    StopSearchButton.Enabled = false;
                }));

                CurrentTime.Stop();
            });
        }

        private void UpdateStatusLabel()
        {
            TimeSpan TimeGone = TimeSpan.FromMilliseconds(CurrentTime.ElapsedMilliseconds);
            ResultsFastTree.Invoke(new MethodInvoker(() =>
            {
                StatusLabel.Text = $"What is going on now:\n\nТекущий статус: {CurrentStatus}\nПрошло времени: {TimeGone.ToString(@"hh\:mm\:ss\:fff")}\nТекущий файл: {CurrentPath}\nОбработано файлов: {CurrentFound}";
            }));
        }

        private void StartDirectorySelectButton_Click(object sender, EventArgs e)
        {
            using (var of_d = new FolderBrowserDialog())
            {
                if (of_d.ShowDialog() == DialogResult.OK)
                {
                    StartDirectoryTextBox.Text = of_d.SelectedPath;
                }
            }
        }

        private void SaveState()
        {
            //сохраняем поля поиска в настройках приложеньки
            Settings.Default["FindName"] = FileNameTextBox.Text;
            Settings.Default["FindDirectory"] = StartDirectoryTextBox.Text;
            Settings.Default["FindText"] = TextInsideTextBox.Text;
            if (asciiRadioButton.Checked) Settings.Default["ASCI"] = true;
            else Settings.Default["ASCI"] = false;
            Settings.Default.Save();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveState();
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }
    }

    //Элемент файловой системы
    public class Node : IEnumerable<Node>
    {
        private List<Node> nodes;
        //Файл? а может и не файл
        public bool IsFile { get; set; }
        //Полный путь
        public string FullPath { get; set; }
        //Имя
        public string Name { get { return Path.GetFileName(FullPath); } }

        // Имеет хоть один файл у себя или у потомков ?
        public bool HasFile
        {
            get
            {
                return IsFile || Nodes.Any(n => n.HasFile);
            }
        }

        public Node()
        {
            nodes = new List<Node>();
        }

        public override string ToString()
        {
            return string.IsNullOrEmpty(Name) ? FullPath : Name;
        }

        //добавление нода
        public void Add(Node node)
        {
            nodes.Add(node);
        }

        IEnumerable<Node> Nodes
        {
            get
            {
                for (int i = 0; i < nodes.Count; i++)
                    yield return nodes[i];
            }
        }

        public IEnumerator<Node> GetEnumerator()
        {
            return Nodes.Where(n => n.HasFile).GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}